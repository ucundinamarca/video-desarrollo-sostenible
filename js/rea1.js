/**
 * @author Edilson Laverde Molina
 * @email edilsonlaverde_182@hotmail.com
 * @create date 2020-12-12 18:45:48
 * @modify date 2020-12-18 09:48:07
 * @desc Videos REA3
 */
var videos = [{
    "id": "iCXBODIc_J0",
    "titulo": "",
    "descripcion": "",
    "url": "iCXBODIc_J0",
    "preguntas": [
        {
            "second": "35",
            "question": "¿Que sensación te hace sentir?",
            "answers": ["Frío", "Bienestar", "Hambre"],
        
            "correct": 2,
            "msg-correct": "Muy bien!!",
            "msg-incorrect": "Te sugerimos repasar el video!!"
        }
    
    ]
}
];    

